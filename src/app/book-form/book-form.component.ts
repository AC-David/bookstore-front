import { finalize } from 'rxjs/operators';
import { BookService } from './../service/api/book.service';
import { Router } from '@angular/router';
import { Book } from './../service/model/book';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bs-book-form',
  templateUrl: './book-form.component.html',
  styles: [
  ]
})

export class BookFormComponent implements OnInit {

    book = <Book>{};

  constructor(private router: Router, private bookService: BookService) { }

  ngOnInit(): void {
  }

  create() {
    this.bookService.createBook(this.book)
      .pipe(finalize(()=>this.router.navigate(['/book-list'])))
      .subscribe();
  }

}
