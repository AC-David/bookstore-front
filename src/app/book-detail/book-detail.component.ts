import { map } from 'rxjs/operators';
import { BookService } from './../service/api/book.service';
import { Book } from './../service/model/book';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { finalize  } from 'rxjs/operators';

@Component({
  selector: 'bs-book-detail',
  templateUrl: './book-detail.component.html',
  styles: [
  ]
})
export class BookDetailComponent implements OnInit {

  book = <Book>{};

  constructor(private router: Router, private bookService: BookService, private route: ActivatedRoute) {
    
   }

  ngOnInit(): void {
    this.route.params
      .pipe(map(params => params['bookId']))
      .pipe(switchMap(id => this.bookService.getBook(id)))
      .subscribe(book => this.book = book);
  }

    delete() {
      this.bookService.deleteBook(this.book.id!)
        .pipe(finalize(() => this.router.navigate(['/book-list'])))
        .subscribe();
    }
}
