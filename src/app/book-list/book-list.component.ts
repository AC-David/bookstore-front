import { Book } from './../service/model/book';
import { BookService } from './../service/api/book.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'bs-book-list',
  templateUrl: './book-list.component.html',
  styles: []
})
export class BookListComponent implements OnInit {

    nbBooks: number = 7;
    books: Book[] = [];

  constructor( private bookService: BookService ) {
    
  }

  ngOnInit(): void {
    this.bookService.countBooks().subscribe(nbBooks => this.nbBooks = nbBooks);
    this.bookService.getBooks().subscribe(books => this.books = books);
  }

}
